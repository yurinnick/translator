﻿namespace Translator
{
    partial class TestForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button1 = new System.Windows.Forms.Button();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.ProgramView = new System.Windows.Forms.RichTextBox();
            this.OpenProgramFile = new System.Windows.Forms.Button();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.OpenDictionaryFile = new System.Windows.Forms.Button();
            this.openFileDialog2 = new System.Windows.Forms.OpenFileDialog();
            this.DictionaryView = new System.Windows.Forms.RichTextBox();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(315, 91);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(234, 28);
            this.button1.TabIndex = 2;
            this.button1.Text = "Analyze";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(21, 326);
            this.textBox2.Multiline = true;
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(257, 104);
            this.textBox2.TabIndex = 1;
            // 
            // textBox3
            // 
            this.textBox3.Location = new System.Drawing.Point(326, 326);
            this.textBox3.Multiline = true;
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new System.Drawing.Size(234, 104);
            this.textBox3.TabIndex = 3;
            // 
            // ProgramView
            // 
            this.ProgramView.Location = new System.Drawing.Point(21, 44);
            this.ProgramView.Name = "ProgramView";
            this.ProgramView.Size = new System.Drawing.Size(257, 250);
            this.ProgramView.TabIndex = 4;
            this.ProgramView.Text = "";
            // 
            // OpenProgramFile
            // 
            this.OpenProgramFile.Location = new System.Drawing.Point(315, 44);
            this.OpenProgramFile.Name = "OpenProgramFile";
            this.OpenProgramFile.Size = new System.Drawing.Size(112, 29);
            this.OpenProgramFile.TabIndex = 5;
            this.OpenProgramFile.Text = "Open Program";
            this.OpenProgramFile.UseVisualStyleBackColor = true;
            this.OpenProgramFile.Click += new System.EventHandler(this.OpenProgramFile_Click);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            this.openFileDialog1.FileOk += new System.ComponentModel.CancelEventHandler(this.openFileDialog1_FileOk);
            // 
            // OpenDictionaryFile
            // 
            this.OpenDictionaryFile.Location = new System.Drawing.Point(443, 44);
            this.OpenDictionaryFile.Name = "OpenDictionaryFile";
            this.OpenDictionaryFile.Size = new System.Drawing.Size(106, 29);
            this.OpenDictionaryFile.TabIndex = 6;
            this.OpenDictionaryFile.Text = "Open Dictionary";
            this.OpenDictionaryFile.UseVisualStyleBackColor = true;
            this.OpenDictionaryFile.Click += new System.EventHandler(this.OpenDictionaryFile_Click);
            // 
            // openFileDialog2
            // 
            this.openFileDialog2.FileName = "openFileDialog2";
            this.openFileDialog2.FileOk += new System.ComponentModel.CancelEventHandler(this.openFileDialog2_FileOk);
            // 
            // DictionaryView
            // 
            this.DictionaryView.Location = new System.Drawing.Point(315, 150);
            this.DictionaryView.Name = "DictionaryView";
            this.DictionaryView.Size = new System.Drawing.Size(245, 144);
            this.DictionaryView.TabIndex = 7;
            this.DictionaryView.Text = "";
            // 
            // TestForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(597, 477);
            this.Controls.Add(this.DictionaryView);
            this.Controls.Add(this.OpenDictionaryFile);
            this.Controls.Add(this.OpenProgramFile);
            this.Controls.Add(this.ProgramView);
            this.Controls.Add(this.textBox3);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.textBox2);
            this.Name = "TestForm";
            this.Text = "TestForm";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.RichTextBox ProgramView;
        private System.Windows.Forms.Button OpenProgramFile;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.Button OpenDictionaryFile;
        private System.Windows.Forms.OpenFileDialog openFileDialog2;
        private System.Windows.Forms.RichTextBox DictionaryView;
    }
}

