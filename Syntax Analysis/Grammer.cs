﻿using System.Collections.Generic;
using Translator.Common;
using Translator.Utilites;

namespace Translator.Syntax_Analysis
{
    public class Grammer
    {
        private List<Rule> grammer = new List<Rule>();

        /// <summary>
        /// Add new rule to grammer
        /// </summary>
        /// <param name="symbol">Name of rule</param>
        /// <param name="items">Rule items</param>
        public void AddRule(string symbol, params Token[] items)
        {
            if (symbol == null)
            {
                throw new System.ArgumentNullException("symbol", "Symbol not set");
            }

            Rule temp = new Rule(symbol, items);

            grammer.Add(temp);
        }
    }
}
