﻿using System.Collections.Generic;
using Translator.Common;

namespace Translator.Syntax_Analysis
{
    public class Rule
    {
        private string Symbol;

        private LinkedList<Token> rules = new LinkedList<Token>();

        public Rule(string symbol, params Token[] items)
        {
            this.Symbol = symbol;
            foreach (Token item in items)
            {
                rules.AddFirst(item);
            }
        }

    }
}
