﻿using System.Collections.Generic;
using Translator.Common;
using Translator.Utilites;

namespace Translator.Lexical_Analysis
{
    /// <summary>
    /// Class for lexical analysis of string
    /// and convertion if to stream of tokens
    /// </summary>
    public class LexAnalyzer
    {
        private string input;

        private Dictionary<string, TokenType> keyWords;

        public LexAnalyzer()
        {
            InitDict();
        }

        public LexAnalyzer(string input)
        {
            this.input = input;
            InitDict();
        }

        public LexAnalyzer(Dictionary<string, TokenType> dictionary)
        {
            keyWords = dictionary;
        }

        public LexAnalyzer(string input, Dictionary<string,TokenType> dictionary)
        {
            this.input = input;
            this.keyWords = new Dictionary<string, TokenType>(dictionary);
        }

        public void InitDict()
        {
            keyWords = new Dictionary<string, TokenType>();
            keyWords.Add("program", TokenType.System);
            keyWords.Add("var", TokenType.System);
            keyWords.Add("begin", TokenType.System);
            keyWords.Add("end", TokenType.System);
            keyWords.Add("while", TokenType.System);
            keyWords.Add("do", TokenType.System);
            keyWords.Add("integer", TokenType.System);
            keyWords.Add("+", TokenType.Operator);
            keyWords.Add("-", TokenType.Operator);
            keyWords.Add("*", TokenType.Operator);
            keyWords.Add("<", TokenType.Bool_oper);
            keyWords.Add(">", TokenType.Bool_oper);
            keyWords.Add("=", TokenType.Bool_oper);
            keyWords.Add(":", TokenType.Separator);
            keyWords.Add(";", TokenType.Separator);
            keyWords.Add(",", TokenType.Separator);
            keyWords.Add(".", TokenType.Separator);
            keyWords.Add("!=", TokenType.Bool_oper);
            keyWords.Add(":=", TokenType.Assignment);

        }

        /// <summary>
        /// Convert strings to tokens, based on setted rules
        /// </summary>
        /// <param name="errors">List of convertation errors</param>
        /// <returns>List of tokens</returns>
        public List<Token> GetTokenStream(out List<string> errors)
        {
            var tempres = GetStringStream(input, out errors);
            return GetTokenStream(tempres, keyWords, out errors);
        }

        /// <summary>
        /// Convert strings to tokens, based on setted rules
        /// </summary>
        /// <param name="StringStream">Strings to convert</param>
        /// <param name="errors">List of convertation errors</param>
        /// <returns>List of tokens</returns>
        public List<Token> GetTokenStream(List<string> StringStream, out List<string> errors)
        {
            return GetTokenStream(StringStream, keyWords, out errors);
        }

        /// <summary>
        /// Convert strings to tokens, based on setted rules
        /// </summary>
        /// <param name="StringStream">Strings to convert</param>
        /// <param name="KeyWords">Dictionary of rules</param>
        /// <param name="errors">List of convertation errors</param>
        /// <returns></returns>
        public List<Token> GetTokenStream(List<string> StringStream, Dictionary<string,TokenType> Dict, out List<string> errors)
        {
            errors = new List<string>();

            List<Token> tokenStream = new List<Token>();

            if (StringStream == null)
            {
                errors.Add("Input string stream is empty");
                return tokenStream;
            }

            if (Dict == null)
            {
                errors.Add("Dictionary not found");
                return tokenStream;
            }

            foreach (var item in StringStream)
            {
                tokenStream.Add(Token.TransformToToken(item, Dict));
            }
            return tokenStream;
        }

        /// <summary>
        /// Separates words and special symbols from input string
        /// </summary>
        /// <param name="errors">List of processing</param>
        /// <returns>List of separated words and symbols</returns>
        public List<string> GetStringStream(out List<string> errors)
        {
            errors = new List<string>();
            return GetStringStream(input, out errors);
        }

        /// <summary>
        /// Separates words and special symbols from input string
        /// </summary>
        /// <param name="str">String to separate</param>
        /// <param name="errors">List of processing errors</param>
        /// <returns>List of separate words and symbols</returns>
        public List<string> GetStringStream(string str, out List<string> errors)
        {
            List<string> stringStream = new List<string>();

            errors = new List<string>();

            if (str == null || str.Equals(string.Empty))
            {
                errors.Add("Input string is empty");
                return stringStream;
            }

            string tempStr;         

            for (int i = 0; i < str.Length; i++)
            {
                tempStr = string.Empty;

                if (!Utiles.isSeparator(str[i]) && 
                    !Utiles.isSpace(str[i]))
                {
                    while ((i < str.Length) && 
                        (!Utiles.isSeparator(str[i]) &&
                        !Utiles.isSpace(str[i])))
                    {
                        tempStr += str[i];
                        i++;
                        
                    }
                    i--;
                }
                else
                {
                    if ((i + 1 < str.Length) && 
                        (str[i].Equals(':') || str[i].Equals('!')) &&
                        str[i + 1].Equals('='))
                    {
                        tempStr = str[i].ToString() + str[i + 1].ToString();
                        i++;
                    }
                    else
                    {
                        if (!Utiles.isSpace(str[i].ToString()))
                        {
                            tempStr = str[i].ToString();
                        }
                    }                      
                }

                if (!tempStr.Equals(string.Empty))
                {
                    stringStream.Add(tempStr);
                }
                
            }
            return stringStream;
        }
    }
}
