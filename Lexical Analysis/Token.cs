﻿namespace Translator.Lexical_Analysis
{
    public class Token
    {
        public TokenType Type { get; set; }
        public string Value { get; set; }

        public Token(string data, TokenType type)
        {
            this.Value = data;
            this.Type = type;
        }

        public enum TokenType
        {
            Operator,
            Bool_oper,
            Const,
            Name,
            System,
            Assignment,
            Separator,
            Unknown
        }
    }
}
