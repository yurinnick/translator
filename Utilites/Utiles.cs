﻿namespace Translator.Utilites
{
    public class Utiles
    {
        /// <summary>
        /// Check if symbol equals to :;=+-*!.,
        /// </summary>
        /// <param name="e">Symbol to check</param>
        /// <returns>True - if equales, False - of not</returns>
        public static bool isSeparator(char e)
        {
            return isSeparator(e.ToString());
        }

        /// <summary>
        /// Check if symbol equals to any space symbol
        /// </summary>
        /// <param name="e">Symbol to check</param>
        /// <returns>True - if equales, False - of not</returns>
        public static bool isSpace(char e)
        {
            return isSpace(e.ToString());
        }

        /// <summary>
        /// Check if symbol is a number
        /// </summary>
        /// <param name="e">Symbol to check</param>
        /// <returns>True - if equales, False - of not</returns>
        public static bool isNumeric(char e)
        {
            return char.IsNumber(e);
        }

        /// <summary>
        /// Check if string equals to :;=+-*!.,
        /// </summary>
        /// <param name="e">String to check</param>
        /// <returns>True - if equales, False - of not</returns>
        public static bool isSeparator(string e)
        {
            if ((e.Equals("<") || e.Equals(">") || 
                e.Equals("=") || e.Equals("+") ||
                e.Equals("-") || e.Equals("*") ||
                e.Equals("!") ||
                e.Equals(":") || e.Equals(";") ||
                e.Equals(".") || e.Equals(",")))
            {
                return true;
            }
            return false;
        }

        /// <summary>
        /// Check if string equals to any space symbol
        /// </summary>
        /// <param name="e">String to check</param>
        /// <returns>True - if equales, False - of not</returns>
        public static bool isSpace(string e)
        {
            if (e.Equals("\t") || e.Equals("\n") ||
                e.Equals(" ") || e.Equals("\r"))
            {
                return true;
            }
            return false;
        }

        /// <summary>
        /// Check if string is a number
        /// </summary>
        /// <param name="e">String to check</param>
        /// <returns>True - if equales, False - of not</returns>
        public static bool isNumeric(string e)
        {
            bool isnumber = true;
            foreach (char c in e)
            {
                isnumber = char.IsNumber(c);
                if (!isnumber)
                {
                    return isnumber;
                }
            }
            return isnumber;
        }
    }
}
