﻿namespace Translator.Common
{
    public enum TokenType
    {
        Operator,
        Bool_oper,
        Const,
        Name,
        System,
        Assignment,
        Separator,
        Rule_ref,
        Unknown
    }
}
