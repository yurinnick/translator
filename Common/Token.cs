﻿using System.Collections.Generic;
using Translator.Utilites;

namespace Translator.Common
{
    /// <summary>
    /// Class declares Token type of elements, which
    /// specifies compliance between input element
    /// and element type
    /// </summary>
    public class Token
    {
        public TokenType Type { get; set; }
        public string Value { get; set; }

        public Token(string data, TokenType type)
        {
            this.Value = data;
            this.Type = type;
        }
        
        /// <summary>
        /// Method creates new Token from string,
        /// based on dictionary and static rules
        /// </summary>
        /// <param name="item">
        /// Input string
        /// </param>
        /// <param name="keyWords">
        /// Dictionary, which containes special elements and there types
        /// </param>
        /// <returns></returns>
        public static Token TransformToToken(string item, Dictionary<string, TokenType> keyWords)
        {
            Token TempToken;
            
            if (keyWords.ContainsKey(item))
                {
                    TempToken = new Token(item, keyWords[item]);
                }
                else
                {
                    if (Utiles.isNumeric(item))
                    {
                        TempToken = new Token(item, TokenType.Const);
                    }
                    else
                    {
                        if (!Utiles.isSpace(item) && 
                            !Utiles.isSeparator(item))
                        {
                            TempToken = new Token(item, TokenType.Name);
                        }
                        else TempToken = new Token(item, TokenType.Unknown);
                    }
                }
            return TempToken;
        }
    }
}
