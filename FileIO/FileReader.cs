﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Translator.Common;
using Translator.Syntax_Analysis;

namespace Translator.FileIO
{
    public class FileReader
    {
        /// <summary>
        /// Read file and add to dictionary values and there types
        /// File format:
        /// TokenValue TokenType
        /// </summary>
        /// <param name="filepath"> Path to file </param>
        /// <param name="errors"> List of processing errors</param>
        /// <returns></returns>
        public static Dictionary<string, TokenType> GetDictionary(string filepath, out List<string> errors)
        {
            errors = new List<string>();

            Dictionary<string, TokenType> outputDict = new Dictionary<string, TokenType>();
            string[] dict_str = null;
            try
            {
                dict_str = System.IO.File.ReadAllLines(filepath);
            }
            catch (System.IO.FileNotFoundException ex)
            {
                errors.Add(ex.ToString());
            }

            for (int i = 0; i < dict_str.Length; i++)
            {
                string[] temp = dict_str[i].Split(' ');
                if (temp.Length == 2)
                {
                    TokenType temp_type = new TokenType();

                    bool status;

                    temp_type = DetectType(temp[1], out status);

                    if (status == true)
                    {
                        outputDict.Add(temp[0],temp_type);
                    }
                    else
                    {
                        errors.Add("Can't recognize token type in " + i.ToString() + " line");
                    }
                }
                else
                {
                    errors.Add("Wrong format in " + i.ToString() + " line");
                }
            }

            return outputDict;
        }

        /// <summary>
        /// Detects types by there names
        /// </summary>
        /// <param name="item"> Typename</param>
        /// <param name="allOk"> Ending function status</param>
        /// <returns> TokenType</returns>
        private static TokenType DetectType(string item, out bool allOk)
        {
            allOk = true;
            switch (item)
            {
                case "Operator": { return TokenType.Operator; }
                case "Assigment": { return TokenType.Assignment; }
                case "Bool_oper": { return TokenType.Bool_oper; }
                case "Separator": { return TokenType.Separator; }
                case "System": { return TokenType.System; }
                default: 
                    {
                        allOk = false;
                        return TokenType.Unknown;
                    }
            }
        }

        /// <summary>
        /// Read program text from file
        /// </summary>
        /// <param name="filepath"> Path to file</param>
        /// <param name="status"> Ending function status</param>
        /// <returns> Program text string</returns>
        public static string GetProgram(string filepath, out bool status)
        {
            status = true;
            string program;
            try
            {
                program = System.IO.File.ReadAllText(filepath);
            }
            catch
            {
                status = false;
                return null;
            }
            return program;
        }

        /// <summary>
        /// Read grammer from file
        /// File format:
        /// Rule_name_1 -> {TokenValue, TokenType}...{TokenValue, TokenType}
        ///                {TokenValue, TokenType}...{TokenValue, TokenType}
        /// 
        /// Rule_name_1 -> {Value,Type}#{Vale,Type}|Value,Type#Vale,Type
        /// 
        /// Rule_name_2 -> ... 
        /// </summary>
        /// <param name="filepath"> Path to file</param>
        /// <param name="errors"> List of processing errors</param>
        /// <returns> Grammer of rules</returns>
        public static Grammer GetGrammer(string filepath, out List<string> errors)
        {
            errors = new List<string>();
            Grammer grammer = new Grammer();


            return grammer;
        }
    }
}
