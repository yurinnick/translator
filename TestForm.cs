﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Translator.FileIO;
using Translator.Common;

namespace Translator
{
    public partial class TestForm : Form
    {
        public TestForm()
        {
            InitializeComponent();
        }

        Lexical_Analysis.LexAnalyzer analyzer = new Lexical_Analysis.LexAnalyzer();
        string program_text;
        Dictionary<string, TokenType> dict;

        private void button1_Click(object sender, EventArgs e)
        {
            if (program_text == string.Empty)
            {
                textBox2.Text = "Error! No program or empty program open!";
            }

            if (dict == null)
            {
                textBox2.Text = "Error No dictionary open!";
            }
            textBox2.Clear();
            textBox3.Clear();

            List<string> errors = new List<string>();

            var str_result = analyzer.GetStringStream(program_text, out errors);

            if (errors.Count != 0)
            {
                foreach (var error in errors)
                {
                    textBox2.Text += error + "\r";
                }
            }
            else
            {
                
                foreach (var item in str_result)
                {
                    textBox2.Text += "[" + item + "] ";
                }
            }

            var result = analyzer.GetTokenStream(str_result, dict,out errors);

            if (errors.Count != 0)
            {
                foreach(var error in errors)
                {
                    textBox3.Text += error + "\r";
                }
            }
            else
            {
                foreach (var item in result)
                {

                    textBox3.Text += "[" + item.Value + " " + item.Type.ToString() + "]_";
                }
            }
        }

        private void OpenProgramFile_Click(object sender, EventArgs e)
        {
            openFileDialog1.ShowDialog();
        }

        private void openFileDialog1_FileOk(object sender, CancelEventArgs e)
        {
            bool status;
            try
            {
                program_text = FileReader.GetProgram(openFileDialog1.FileName, out status);
                ProgramView.Text = program_text;
            }
            catch
            {
                textBox2.Text = "Error reading file";
            }
        }

        private void OpenDictionaryFile_Click(object sender, EventArgs e)
        {
            openFileDialog2.ShowDialog();
        }

        private void openFileDialog2_FileOk(object sender, CancelEventArgs e)
        {
            List<string> errors = new List<string>();
            bool status;
            dict = FileReader.GetDictionary(openFileDialog2.FileName, out errors);
            DictionaryView.Text = FileReader.GetProgram(openFileDialog2.FileName, out status);
        }
    }
}
